# AUI UMD example

Demonstrates how to consume AUI through UMD, as of AUI 5.9.0

Enables piecemeal consumption.

Check out `dist/{browserify,requirejs}.index.html` for examples of each!

* `src/requirejs.main.js` is the main entrypoint for requirejs
* `dist/js/requirejs.main.js` is the compiled requirejs bundle

* `src/browserify.index.js` is the main entrypoint for browserify
* `dist/js/browserify.bundle.js` is the compiled browserify bundle

# AUI-ADG or AUI?

The examples here use [AUI-ADG](https://bitbucket.org/atlassian/aui-adg), which requires you to log in to the Atlassian private NPM registry

```
npm login --registry=https://npm-private.atlassian.io --scope=atlassian
```

Use your crowd / LDAP credentials.

If you do not have access to this, you can use the [AUI npm module](https://www.npmjs.com/package/@atlassian/aui). This does not require you to log in.
Using this is the same as using AUI-ADG.

# Caveats

* You must shim `jquery` when using RequireJS – it's BYO jQuery version, so we will define dependencies on this

* CSS is not yet consumable as individual files. We recommend using the built CSS files for now. You may use the individual CSS files, but there may be extra (implicit) CSS dependencies for each component

# License

* See the LICENSE file for details
